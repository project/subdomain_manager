<?php
/*

Class Name: cpsubdomain
Clas Title: cPanel Subdomains Creator
Purpose: Create cPanel subdomains without logging into cPanel.
Version: 1.0
Author: Md. Zakir Hossain (Raju)
URL: http://www.rajuru.xenexbd.com

Company: Xenex Web Solutions
URL: http://www.xenexbd.com

License: LGPL
You can freely use, modify, distribute this script. But a credit line is appreciated.

Installation:
see example.php for details

Compatibility: PHP4
*/

//definding main class
class cpsubdomain{
  //declare public variables
  var $cpuser;    // cPanel username
  var $cppass;        // cPanel password
  var $cpdomain;      // cPanel domain or IP  
  var $cpextension;
  var $cpskin;        // cPanel skin. Mostly x or x2. http://gloscon.com:2082/frontend/xpevolution/index.php 
  							// subdomain/index.php    http://gloscon.com:2082/frontend/xpevolution/subdomain/index.php
							// It could be "x2", "rvblue", etc.
  //defining constructor
  function cpsubdomain($cpuser,$cppass,$cpdomain,$cpskin='x',$cpextension='html'){  
    $this->cpuser=$cpuser;
	$this->cppass=$cppass;
	$this->cpdomain=$cpdomain;
	$this->cpskin=$cpskin;
	$this->cpextension = $cpextension;  /* added by deepak */
	// See following URL to know how to determine your cPanel skin
	// http://www.zubrag.com/articles/determine-cpanel-skin.php
  }
  
/*
<script language="JavaScript">
<!-- Begin
function validate(field) {
var valid = "abcdefghijklmnopqrstuvwxyz0123456789-"
var ok = "yes";
var temp;
for (var i=0; i<field.value.length; i++) {
temp = "" + field.value.substring(i, i+1);
if (valid.indexOf(temp) == "-1") ok = "no";
}
if (ok == "no") {
alert("Invalid entry!  Only characters and numbers are accepted!");
field.focus();
field.select();
return false;
   }
}
//  End -->
</script>

<form action="doadddomain.php" onsubmit="return validate(this.domain)">
<input name="domain" onchange="return validate(this)" type="text">.<select name="rootdomain">
<option>gloscon.com</option>
</select>
<input class="form_submit" value="Add" type="submit">

<br><font size="-2">Note: If you wish to grant virtual ftp access to this subdomain, just create an FTP user with the same username as the subdomain base name that is listed inside the () above.</font>
</form>
*/
  //function for creating subdomain
  
  function createSD($esubdomain){
  	//checking whether the subdomain is exists 
  	$subdomain=$esubdomain.".".$this->cpdomain;
  	$path="http://".$this->cpuser.":".$this->cppass."@".$this->cpdomain.":2082/frontend/".$this->cpskin."/subdomain/index.".$this->cpextension;
	//echo $path;
	//exit;
	// we can also use curl here to do this in a better way 
	$f = fopen($path,"r");
	if (!$f) {
	  return('Can\'t open cPanel');
	}

    //check if the account exists
    while (!feof ($f)) {          // parse page and find already domain from listing 
	  $line = fgets ($f, 1024);
	  if (ereg ($subdomain, $line, $out)) {
	    return('Such subdomain already exists.');
	  }
	}
	fclose($f); //close the file resource
	
	
	//subdomain does not already exist. So proceed to creating it
    $path="http://".$this->cpuser.":".$this->cppass."@".$this->cpdomain.":2082/frontend/".$this->cpskin."/subdomain/doadddomain.".$this->cpextension."?domain=".$esubdomain."&rootdomain=".$this->cpdomain;
    $f = fopen($path,"r");
	if (!$f) {
	  return('Can\'t open cPanel.');
	}

    //check if the subdomain added
    while (!feof ($f)) { 
	  $line = fgets ($f, 1024);
	  if (ereg ("has been added.", $line, $out)) {
	    return('Subdomain created successfully');
	  }
	}
	fclose($f); //close the file resource
    //return success message 
	return "There may be some error while creating subdomain.";
	
 }
 function deleteSD($esubdomain){
 	
 	$del_flg = false;
 	
  	//checking whether the subdomain is exists 
  	$subdomain=$esubdomain.".".$this->cpdomain;
  	$path="http://".$this->cpuser.":".$this->cppass."@".$this->cpdomain.":2082/frontend/".$this->cpskin."/subdomain/index.".$this->cpextension;
	//echo $path;
	//exit;
	// we can also use curl here to do this in a better way 
	$f = fopen($path,"r");
	if (!$f) {
	  return('Can\'t open cPanel');
	}

    //check if the account exists
    while (!feof ($f)) {          // parse page and find already domain from listing 
	  $line = fgets ($f, 1024);
	  if (ereg ($subdomain, $line, $out)) {
	      // make del falg true 
	      $del_flg = true;
	  }
	}
   
	if( $del_flg == false ) { 
	  return('Such subdomain does not exists.');
   }
	  
	fclose($f); //close the file resource
	
	$deldomain=$esubdomain."_".$this->cpdomain;  // gujjuweb_gloscon.com gujjuweb.gloscon.com
	
	//subdomain does not already exist. So proceed to creating it
    $path="http://".$this->cpuser.":".$this->cppass."@".$this->cpdomain.":2082/frontend/".$this->cpskin."/subdomain/dodeldomain.".$this->cpextension."?domain=".$deldomain;
    $f = fopen($path,"r");
	if (!$f) {
	  return('Can\'t open cPanel.');
	}

    //check if the subdomain added
    while (!feof ($f)) { 
	  $line = fgets ($f, 1024);
	  
	  /* SubDomain Removal
Removed Entry from httpd.conf The subdomain, ztestsubdomain1.gloscon.com has been removed. */

	  if (ereg ("has been removed.", $line, $out)) {
	    return('Subdomain removed successfully');
	  }
	}
	fclose($f); //close the file resource
    //return success message 
	return "There may be some error while deleting subdomain.";
	
 }
 
}

?>